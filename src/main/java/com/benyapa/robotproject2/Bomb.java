/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.robotproject2;

/**
 *
 * @author bwstx
 */
public class Bomb {

    private int x;
    private int y;
    private char sym = 'b';

    public Bomb(int x, int y) {
        this.x = x;
        this.y = y;
        this.sym = 'b';
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getsym() {
        return sym;
    }
    public boolean isOn(int x, int y){
        return this.x == x && this.y == y;
    }
}
